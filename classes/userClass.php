<?php
/**
 * userClass.php
 * Core Support for user system
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 12-Mar-2015
 * @updated 17-Nov-2016
 * @package userClass
 **/

//(this library adds the PHP 5.5 password hashing functions to older versions of PHP)
if( version_compare( PHP_VERSION, '5.5.0', '<' ) )
{
    require_once( dirname( __FILE__ ) . '/password_compatibility.php' );
}

class userClass {
    
    private $database;
    private $classpath;
    private $options = array(
        
        //require email conf. for normal reg?
        'confirm_users' => false,

        //require email conf. for social media auth?
        'confirm_social_users' => false, 
        
        //send users a reg success email?
        'send_reg_success' => true,

        //database table name to use for users
        'users_table' => 'users',  

        //Force header() location redirect?           
        'header_redirect' => false,

        //Reg with email OR username/email
        'email_only' => true,
        
        //If Crypt functions are availble, use them?
        'use_crypt' => true,

        //Use PHP 5 >= 5.5.0, PHP 7 password_hash algos
        'use_pwd_hash' => false,

        //Trailing slash IS important!
        'url' => 'http://yoururl.com/',

        //Multiplier for password hashing
        'hash_cost_factor' => 10,

        //2 weeks for secure cookie
        'cookie_runtime' => 1209600, 

        // important: always put a dot in front of the domain, like ".mydomain.com" see http://stackoverflow.com/q/9618217/1114320
        'cookie_domain' => '.127.0.0.1', 

        //Name of cookie to set
        'cookie_name' => 'remember_me', 

        //Outbound email "from" email
        'from_email' => 'noreply@yoursite.com', 

        //Outbound email "from" name
        'from_name' => 'yoursite', 

        //Full URI to where you plan on having the activate_account function
        'email_verification_url' => 'login/verify', 

        //Subject line for account activation emails
        'email_verification_subject' => 'account activation for project xy', 
        
        //Subject line for account creation email (sent when users register and don't have to activate)
        'reg_success_subject' => 'Account created successfully', 

        //Password reset URI where verify_pass_reset() will be
        'email_password_reset_url' => 'login/verifypasswordreset', 

        //Password reset email subject
        'email_password_reset_subject' => 'password reset for project xy', 

        //Logout redirect for page_protect()
        'secure_redirect' => 'users/login', 

        //Oauth credentials
        'oauth' => array(
            'fb' => array(
                'client_id' => '', 
                'client_secret' => '', 
                'client_redirect' => ''
            ), 
            'google' => array(
                'client_id' => '', 
                'client_secret' => '', 
                'client_redirect' => ''
            )
        ), 
        
        //User levels
        'user_levels' => array(
            'user' => 1, 
            'admin' => 5
        ),

        //Email messaging copy
        'message_copy' => array(

            //Registration without confirmation
            'registration' => '',

            //Registration WITH confirmation
            'registration_confirm' => '',

            //Password reset messages
            'password_reset' => '',

        ),

        //Copy => variable replacements for message copy above
        'message_replacements' => array(

        ),
    );


	/**
	 * userClass constructor.
	 *
	 * @param SimplePDO $database
	 * @param array $options
	 */
    public function __construct( $database, $options = array() )
    {
        $this->database = $database;
        $this->classpath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR;
        if( !session_id() || !strlen( session_id() ) )
        {
            session_start();   
        }

        //Set the class options
        $this->set_options( $options );
    }
    //end __construct


	/**
	 * @param $vars
	 * @return $this
	 */
    public function set_options( $vars )
    {
        //Update any options as needed
        foreach( $this->options as $key => $default )
        {
            if( isset( $vars[$key] ) )
            {
                $this->options[$key] = $vars[$key];
            }
            else
            {
                $this->options[$key] = $default;
            }
        }
        
        //Reassign other vars as needed
        if( empty( $this->options['url'] ) )
        {
            $this->options['url'] = 'http://'. $_SERVER['HTTP_HOST'] . DIRECTORY_SEPARATOR;
        }
        return $this;
    }
    
    
    /**
     * Registration function
     * To be processed by normal form $_POST vals
     * Example usage:
     * 
     * $_POST = array(
     *     'user_name' => 'someguy',
     *     'user_email' => 'someone@email.com',  
     *     'user_password' => 'somepassword', 
     *     'user_remember' => true
     * );
     * if( $users->register() )
     * {
     *    echo 'Reg success!'; 
     * }
     *
     *
     * @access public
     * @param array $_POST
     * @return bool
     */
    public function register()
    {
        $errors = array();
        
        //Check the username
        if( !isset( $_POST['user_name'] ) || empty( $_POST['user_name'] ) )
        {
            $errors[] = 'Username is required';
        }
        
        //Check pass
        if( !isset( $_POST['user_password'] ) || empty( $_POST['user_password'] ) )
        {
            $errors[] = 'Password is required';
        }
        
        //Check email
        if( !isset( $_POST['user_email'] ) || empty( $_POST['user_email'] ) )
        {
            $errors[] = 'Email address is required';
        }
        
        //Check username
        if( strlen( $_POST['user_name'] ) > 64 || strlen( $_POST['user_name'] ) < 2 || !$this->user_valid_username( $_POST['user_name'] ) )
        {
            $errors[] = 'Please enter a valid user name';
        }
        
        //Check email
        if( !$this->user_valid_email( $_POST['user_email'] ) )
        {
            $errors[] = 'Please enter a valid email address';
        }
        
        //If the errors aren't empty, return them
        if( !empty( $errors ) )
        {
            return array( 'success' => false, 'errors' => $errors );
        }
        
        //Hash the user pass
        $user_password_hash = $this->user_system_password_hash( $_POST['user_password'] );
        
        //See if the user already exists
        $query = "SELECT user_id FROM ".$this->options['users_table'] ." WHERE ";
        if( !isset( $_POST['user_name'] ) )
        {
            $query .= "LOWER(user_email) = ?";
            
            $cond = array(
                strtolower( trim( $_POST['user_email'] ) )
            );
            
            $username = $this->generate_uniquename( trim( $_POST['user_email'] ) );
        }
        else
        {
            $query .= "(LOWER(user_email) = ? OR user_name = ?)";
            
            $cond = array(
                strtolower( trim( $_POST['user_email'] ) ),
                strtolower( trim( $_POST['user_name'] ) )
            );
            
            $username = $this->generate_uniquename( trim( $_POST['user_name'] ) );
        }

        $count = $this->database->num_rows( $query, $cond );
        
        //If the user does exist, toss error and get out
        if( $count > 0 )
        {
            return array( 'success' => false, 'errors' => array( 'Looks like you\'ve already registered' ) );
        }

        //Get the available fields
        $available_fields = $this->database->list_fields( $this->options['users_table'] );
        
        // generate random hash for email verification (40 char string)
        $activation_hash = sha1( uniqid( mt_rand(), true ) );
        
        $add_user = array(
            'user_name' => $username, 
            'user_email' => strtolower( $_POST['user_email'] ), 
            'user_password' => $user_password_hash, 
            'user_registered' => 'NOW()',
            'user_activation_hash' => $activation_hash, 
            'user_ip' => $this->user_system_get_ip()
        );
        
        //If we don't need to validate users via email, auto approve them
        if( $this->options['confirm_users'] === false )
        {
            $add_user['user_active'] = 1;
        }
        
        //If this is requested by a logged in admin, allow user level to be specified
        if( $this->logged_in() && $this->check_admin() && isset( $_POST['user_level'] ) )
        {
            $add_user['user_level'] = $_POST['user_level'];
        }

        //Now insert whatever remaining stuff is passed if it's available in the users table
        $remaining = array_diff( $available_fields, array_keys( $add_user ) );
        foreach( $_POST as $k => $v )
        {
            if( in_array( $k, $remaining ) )
            {
                $add_user[$k] = $v;
            }
        }
        
        //Insert the userdata
        if( !$this->database->insert( $this->options['users_table'], $add_user ) )
        {
            return array( 'success' => false, 'errors' => array( 'Account creation failed.  Please try again' ) );
        }
        
        //Get the user ID
        $user_id = $this->database->lastid();
        
        if( $this->options['confirm_users'] === true )
        {
            if( !$this->send_confirmation( $user_id, $_POST['user_email'], $activation_hash ) )
            {
                return array( 'success' => false, 'errors' => array( 'Unable to send verification email' ) );
            }
        }
        elseif( $this->options['send_reg_success'] === true || ( isset( $_POST['send_notification'] ) && $_POST['send_notification'] == 1 ) )
        {
            if( !$this->send_reg_notice( $_POST['user_email'], $_POST['user_password'] ) )
            {
                return array( 'success' => false, 'errors' => array( 'Unable to send verification email' ) );
            }
        }
        
        return true;
    }
    //end register()
    
    
    /**
     * Function to either login, or register a facebook user
     * Existing users are just logged in
     * New users are registered
     * @access public
     * @param none
     * @return bool
     */
    public function facebook_authenticate()
    {
        if( empty( $this->options['oauth']['fb']['client_id'] ) || empty( $this->options['oauth']['fb']['client_secret'] ) )
        {
            trigger_error( 'Please go to Facebook Apps page https://developers.facebook.com/apps, create an application, and initialize the userClass with the fb_client_id and fb_client_secret to the App ID/API Key and client_secret with App Secret' );
            return false;
        }
        
        //If the user is already logged in, we don't need to do any of this shit
        if( $this->logged_in() )
        {
            return true;
        }
        
        require_once( $this->classpath . 'oauth/http.php' );
        require_once( $this->classpath . 'oauth/oauth_client.php' );
        
        $user = false;
        
        $client = new oauth_client_class;
        $client->debug = false;
        $client->debug_http = true;

        $client->server = 'Facebook';
        $client->redirect_uri = $this->options['url'] . $this->options['oauth']['fb']['client_redirect'];
        $client->client_id = $this->options['oauth']['fb']['client_id'];
        $client->client_secret = $this->options['oauth']['fb']['client_secret'];
        
        $client->scope = 'email';
        if( $success = $client->Initialize() )
        {
            if( $success = $client->Process() )
            {
                if( strlen( $client->access_token ) )
                {
                    $success = $client->CallAPI(
                        'https://graph.facebook.com/me', 
                        'GET', 
                        array(), 
                        array( 'FailOnAccessError' => true ), 
                        $user
                    );
                }
            }
            $success = $client->Finalize( $success );
        }

        if( $client->exit || $success == false )
        {
            return false;
        }
        
        if( !$user )
        {
            return array( 'success' => false, 'errors' => array( 'No facebook user data' ) );
        }
        
        //Check to make sure we got the fb email
        if( !isset( $user->email ) || empty( $user->email ) )
        {
            return array( 'success' => false, 'errors' => array( 'Email address is required' ) );
        }
        
        //See if the user already exists as a registered user, if so, just login
		$params = array(
			strtolower( $user->email ),
		);

		$fb_query = "SELECT
			user_id,
			user_name,
			user_level,
			user_num_logins,
			user_auth_id,
		 	user_provider_type
		FROM ".$this->options['users_table']." WHERE
			LOWER(user_email) = ?
		LIMIT 1";
		$facebook_user = $this->database->get_row( $fb_query, $params );

        //User exists, login and stop.
        if( !empty( $facebook_user->user_id ) || !empty( $facebook_user->user_level ) )
        {
			//If the user was previously a normal user and is logging in with FB, update their row to reflect the change
			$update = array();
			if( empty( $facebook_user->user_auth_id ) )
				$update['user_auth_id'] = $user->id;
			if( empty( $facebook_user->user_provider_type ) )
				$update['user_provider_type'] = 'facebook';

			if( !empty( $update ) )
			{
				$this->database->update( USERS, $update, array( 'user_id' => $facebook_user->user_id ), 1 );
			}

            //We've gotten this far, let's set up the session and login the user
            $_SESSION = array(
                'user_logged_in' => true, 
                'user_id' => $facebook_user->user_id, 
                'user_name' => $facebook_user->user_name, 
                'user_level' => $facebook_user->user_level, 
                'user_type' => 'facebook'
            );
            
            $num_logins = !empty( $facebook_user->user_num_logins ) ? ( $facebook_user->user_num_logins + 1 ) : 1;

            //Update the last login timestamp
            $this->database->update( $this->options['users_table'], array( 'user_last_login' => 'NOW()', 'user_num_logins' => $num_logins ), array( 'user_id' => $facebook_user->user_id ), 1 );
            
            $this->set_remember_cookie( $facebook_user->user_id );
            
            return true;
        }
        //end user already exists and is now logged in
        
        //User doesn't already exist- register them
        $activation_hash = sha1( uniqid( mt_rand(), true ) );
        $user_name = $this->generate_uniquename( $user->name );
        
        $add_user = array(
            'user_name' => $user_name, 
            'user_email' => strtolower( $user->email ), 
            'user_registered' => 'NOW()',
            'user_activation_hash' => $activation_hash, 
            'user_ip' => $this->user_system_get_ip(), 
            'user_provider_type' => 'facebook', 
            'user_auth_id' => $user->id,
        );
        
        //If we don't need to validate users via email, auto approve them
        if( $this->options['confirm_social_users'] === false )
        {
            $add_user['user_active'] = 1;
        }
        
        //Insert the userdata
        if( !$this->database->insert( $this->options['users_table'], $add_user ) )
        {
            return array( 'success' => false, 'errors' => array( 'Account creation failed.  Please try again.' ) );
        }
        
        //Get the user ID
        $user_id = $this->database->lastid();
        
        //If we didn't pre-approve the user, send them an activation email
        if( !isset( $add_user['user_active'] ) )
        {
            if( !$this->send_confirmation( $user_id, $user->email, $activation_hash ) )
            {
                return array( 'success' => false, 'errors' => array( 'Unable to send verification email.' ) );
            }
        }
        
        if( $add_user['user_active'] == 1 )
        {
            $_SESSION = array(
                'user_logged_in' => true, 
                'user_id' => $user_id, 
                'user_name' => $user_name, 
                'user_level' => 1, 
                'user_type' => 'facebook',
				'user_logins' => 1,
				'first_time' => true,
            );

            //Update the last login timestamp
            if( $this->database->update( $this->options['users_table'], array( 'user_last_login' => 'NOW()', 'user_num_logins' => 1 ), array( 'user_id' => $user_id ), 1 ) )
            {
                $this->set_remember_cookie( $user_id );
                
                if( !headers_sent() )
                {
                    header( 'Location: '. $this->options['url'] . $this->options['oauth']['fb']['client_redirect'] );
                    exit;   
                }
                else
                {
                    return true;
                }
            }
        }
        
        return true;
        
    }
    //end facebook_authenticate()


	/**
	 * @return bool
	 */
    public function google_authenticate()
    {
        if( empty( $this->options['oauth']['google']['client_id'] ) || empty( $this->options['oauth']['google']['client_secret'] ) )
        
        {
            trigger_error( 'Please go to Google APIs console page http://code.google.com/apis/console in the API access tab, create a new client ID, and set the client_id to Client ID and client_secret with Client Secret.  The callback URL must be '.$this->options['url'].' but make sure the domain is valid and can be resolved by a public DNS.' );
            return false;
        }
        
        //If the user is already logged in, we don't need to do any of this shit
        if( $this->logged_in() )
        {
            return true;
        }

        require_once( $this->classpath . 'oauth/http.php' );
        require_once( $this->classpath . 'oauth/oauth_client.php' );

        $user = false;

        $client = new oauth_client_class;
        $client->debug = false;
        $client->debug_http = true;

        $client->server = 'Google';
        $client->redirect_uri = $this->options['url'] . $this->options['oauth']['google']['client_redirect'];
        $client->client_id = $this->options['oauth']['google']['client_id'];
        $client->client_secret = $this->options['oauth']['google']['client_secret'];

        $client->scope = 'https://www.googleapis.com/auth/userinfo.email '. ' https://www.googleapis.com/auth/userinfo.profile';

        if( $success = $client->Initialize() )
        {
            if( $success = $client->Process() )
            {
                if( strlen( $client->authorization_error ) )
                {
                    $client->error = $client->authorization_error;
                    $success = false;
                }
                elseif( strlen( $client->access_token ) )
                {
                    $success = $client->CallAPI(
                        'https://www.googleapis.com/oauth2/v1/userinfo',
                        'GET', 
                        array(), 
                        array( 'FailOnAccessError' => true ), 
                        $user
                    );
                }
            }
            $success = $client->Finalize( $success );
        }

        if( $client->exit )
        {
            exit;
        }
        if( $success === false )
        {
            return array( 'success' => false, 'errors' => array( $client->error ) );
        }

        if( !$user )
        {
            return array( 'success' => false, 'errors' => array( 'No google user data' ) );
        }

        //Check to make sure we got the fb email
        if( !isset( $user->email ) || empty( $user->email ) )
        {
            return array( 'success' => false, 'errors' => array( 'Email address is required' ) );
        }

        //See if the user already exists as a registered user, if so, just login
        $params = array(
            strtolower( $user->email ),
        );

        $google_query = "SELECT
			user_id,
			user_name,
			user_level,
			user_num_logins,
			user_auth_id,
		 	user_provider_type
		FROM ".$this->options['users_table']." WHERE
			LOWER(user_email) = ?
		LIMIT 1";
        $google_user = $this->database->get_row( $google_query, $params );

        //User exists, login and stop.
        if( !empty( $google_user->user_id ) || !empty( $google_user->user_level ) )
        {
			//If the user was previously a normal user and is logging in with google, update their row to reflect the change
			$update = array();
			if( empty( $google_user->user_auth_id ) )
				$update['user_auth_id'] = $user->id;
			if( empty( $google_user->user_provider_type ) )
				$update['user_provider_type'] = 'google';

			if( !empty( $update ) )
			{
				$this->database->update( USERS, $update, array( 'user_id' => $google_user->user_id ), 1 );
			}

            //We've gotten this far, let's set up the session and login the user
            $_SESSION = array(
                'user_logged_in' => true, 
                'user_id' => $google_user->user_id, 
                'user_name' => $google_user->user_name, 
                'user_level' => $google_user->user_level, 
                'user_type' => 'google'
            );

            $num_logins = !empty( $google_user->user_num_logins ) ? ( $google_user->user_num_logins + 1 ) : 1;

            //Update the last login timestamp
            $this->database->update( $this->options['users_table'], array( 'user_last_login' => 'NOW()', 'user_num_logins' => $num_logins ), array( 'user_id' => $google_user->user_id ), 1 );
            
            $this->set_remember_cookie( $google_user->user_id );

            return true;
        }
        //end user already exists and is now logged in

        //User doesn't already exist- register them
        $activation_hash = sha1( uniqid( mt_rand(), true ) );
        $user_name = $this->generate_uniquename( $user->name );

        $add_user = array(
            'user_name' => $user_name, 
            'user_email' => strtolower( $user->email ), 
            'user_registered' => 'NOW()',
            'user_activation_hash' => $activation_hash, 
            'user_ip' => $this->user_system_get_ip(), 
            'user_provider_type' => 'google', 
            'user_auth_id' => $user->id
        );

        //If we don't need to validate users via email, auto approve them
        if( $this->options['confirm_social_users'] === false )
        {
            $add_user['user_active'] = 1;
        }

        //Insert the userdata
        if( !$this->database->insert( $this->options['users_table'], $add_user ) )
        {
            return array( 'success' => false, 'errors' => array( 'Account creation failed.  Please try again.' ) );
        }

        //Get the user ID
        $user_id = $this->database->lastid();

        //If we didn't pre-approve the user, send them an activation email
        if( !isset( $add_user['user_active'] ) )
        {
            if( !$this->send_confirmation( $user_id, $user->email, $activation_hash ) )
            {
                return array( 'success' => false, 'errors' => array( 'Unable to send verification email.' ) );
            }
        }

        if( $add_user['user_active'] == 1 )
        {
            $_SESSION = array(
                'user_logged_in' => true, 
                'user_id' => $user_id, 
                'user_name' => $user_name, 
                'user_level' => 1, 
                'user_type' => 'google',
				'user_logins' => 1,
				'first_time' => true,
            );

            //Update the last login timestamp
            if( $this->database->update( $this->options['users_table'], array( 'user_last_login' => 'NOW()', 'user_num_logins' => 1 ), array( 'user_id' => $user_id ), 1 ) )
            {
                $this->set_remember_cookie( $user_id );
                
                if( !headers_sent() )
                {
                    header( 'Location: '. $this->options['url'] . $this->options['oauth']['google']['client_redirect'] );
                    exit;   
                }
                else
                {
                    return true;
                }
            }
        }

        return true;

    }
    //end google_authenticate()


	/**
	 * @return array|bool
	 */
    public function login()
    {
        //Check the username
        if( !isset( $_POST['user_email'] ) || empty( $_POST['user_email'] ) )
        {
            $message = 'Please provide your username or email';
            if( $this->options['email_only'] )
            {
                $message = 'Email is required';
            }
            return array( 'success' => false, 'errors' => array( $message ) );
        }
        
        //Check pass
        if( !isset( $_POST['user_password'] ) || empty( $_POST['user_password'] ) )
        {
            return array( 'success' => false, 'errors' => array( 'Password is required.' ) );
        }
        
        //Not empty so check the userdata
        $query = "SELECT 
            user_id, 
            user_name,
            user_email,
            user_level, 
            user_password, 
            user_failed_logins, 
            user_last_failed_login, 
            user_active, 
            user_num_logins,
		    user_provider_type
        FROM ".$this->options['users_table'] ." WHERE ";
        
        //IF we've passed a username, use it
        if( isset( $_POST['user_name'] ) )
        {
            $params = array(
                strtolower( trim( $_POST['user_name'] ) ),
                strtolower( trim( $_POST['user_email'] ) )
            );
            $query .= "(LOWER(user_name) = ? OR LOWER(user_email) = ?)";
        }
        else
        {
            $params = array(
                strtolower( trim( $_POST['user_email'] ) )
            );
            $query .= "LOWER(user_email) = ?";
        }

        $query .= " LIMIT 1";
        
        $user = $this->database->get_row( $query, $params );


		//If the user registered via social media and hasn't set a password, we may need to let them know
		$types = array(
			'google',
			'facebook',
		);
		if( !empty( $user->user_provider_type ) && in_array( $user->user_provider_type, $types ) && empty( $user->user_password ) )
		{
			return array(
				'success' => false,
				'errors' => array(
					'Looks like you registered using '. $user->user_provider_type.' and haven\'t yet set a password.  Try logging in with '. $user->user_provider_type.' to set a password on your user settings page, after which you may login using your email address and password.'
				)
			);
		}
        
        //No user, stop.
        if( empty( $user->user_id ) || empty( $user->user_password ) || empty( $user->user_level ) )
        {
            return array( 'success' => false, 'errors' => array( 'Invalid user' ) );
        }
        
        //Too many login attempts for this user.  Fail.
        if( ( $user->user_failed_logins >= 3 ) && ( $user->user_last_failed_login > ( time() - 30 ) ) )
        {
            return array( 'success' => false, 'errors' => array( 'Too many invalid login attempts.  Please try again in a few minutes.' ) );
        }
        
        //Mismtached passwords.  Fail.
        if( !$this->user_check_password( $user->user_password, $_POST['user_password'] ) )
        {
            //Update the db to reflect the fail
            $params = array(
                time(), 
                strtolower( trim( $_POST['user_email'] ) )
            );
            $this->database->query( "UPDATE ".$this->options['users_table']." SET user_failed_logins = user_failed_logins+1, user_last_failed_login = ? WHERE (LOWER(user_email) = ?) LIMIT 1", $params );

            return array( 'success' => false, 'errors' => array( 'Invalid password.' ) );
        }
        
        //Inactive user.
        if( $user->user_active < 1 )
        {
            return array( 'success' => false, 'errors' => array( 'Your account has not been activated.' ) );
        }
        
        //We've gotten this far, let's set up the session and login the user
        $_SESSION = array(
            'user_logged_in' => true, 
            'user_id' => $user->user_id, 
            'user_name' => $user->user_name,
			'user_email' => $user->user_email,
            'user_level' => $user->user_level, 
            'user_type' => 'email',
			'user_logins' => $user->user_num_logins,
        );
        
        //Reset the login attempt counter if $failed_logins > 0
        if( $user->user_failed_logins > 0 )
        {
            $this->database->update( $this->options['users_table'], array( 'user_failed_logins' => 0, 'user_last_failed_login' => 'NULL' ), array( 'user_id' => $user->user_id ), 1 );
        }
        
        //Update the last login timestamp and total login count
        $login_count = ( $user->user_num_logins + 1 );
        $this->database->update( $this->options['users_table'], array( 'user_last_login' => 'NOW()', 'user_num_logins' => $login_count ), array( 'user_id' => $user->user_id ), 1 );
        
        //If recall is desired, set that bad boy
        if( isset( $_POST['user_remember'] ) )
        {
            $this->set_remember_cookie( $user->user_id );
        }
        
        //If a redirect has been posted, and the headers not already sent, do a redirect
        if( !headers_sent() && $this->options['header_redirect'] && isset( $_POST['redirect'] ) && !empty( $_POST['redirect'] ) )
        {
            header( 'Location: '. $_POST['redirect'] );
            exit;
        }
        elseif( $this->options['header_redirect'] === false && isset( $_POST['redirect'] ) && !empty( $_POST['redirect'] ) )
        {
            return array( 'success' => true, 'redirect' => $_POST['redirect'] );
        }
        else
        {
            return true;
        }
    }
    //end login()


	/**
	 * @return array|bool
	 */
    public function cookie_login()
    {
        $cookie = isset( $_COOKIE[$this->options['cookie_name']] ) ? $_COOKIE[$this->options['cookie_name']] : '';
        if( empty( $cookie ) )
        {
            return array( 'success' => false, 'errors' => array( 'Your remember me cookie is invalid.' ) );
        }
        
        list( $user_id, $token, $hash ) = explode( ':', $cookie );
        if( $hash !== hash( 'sha256', $user_id . ':' . $token ) || empty( $token ) )
        {
            return array( 'success' => false, 'errors' => array( 'Your remember me cookie is invalid.' ) );
        }
        
        //Get the user data based on the hash we got from the cookie
        $params = array(
            $user_id, 
            $token
        );
        $user = $this->database->get_row( "SELECT user_id, user_name, user_level, user_email, user_num_logins FROM ".$this->options['users_table']." WHERE user_id = ? AND user_remember_token = ? AND user_remember_token IS NOT NULL LIMIT 1", $params );

        //No user, stop and delete the cookie
        if( empty( $user->user_id ) || empty( $user->user_level ) )
        {
            $this->delete_cookie();
            return array( 'success' => false, 'errors' => array( 'Invalid user' ) );
        }
        
        //We've gotten this far, let's set up the session and login the user
        $_SESSION = array(
            'user_logged_in' => true, 
            'user_id' => $user->user_id, 
            'user_name' => $user->user_name,
			'user_email' => $user->user_email,
            'user_level' => $user->user_level, 
            'user_type' => 'email',
			'user_num_logins' => $user->user_num_logins,
        );
        
        //Update the last login timestamp
        $this->database->update( $this->options['users_table'], array( 'user_last_login' => 'NOW()' ), array( 'user_id' => $user->user_id ), 1 );
        
        return true;
        
    }
    //end cookie_login()


    
    /**
     * Function to update user password
     * Requires both old and new password
     * @access public
     * @param string $_POST['old_password']
     * @param string $_POST['new_password']
     * @return bool
     */
    public function update_password()
    {
        
        if( !isset( $_POST['old_password'] ) || empty( $_POST['old_password'] ) || !isset( $_POST['new_password'] ) || empty( $_POST['new_password'] ) )
        {
            return array( 'success' => false, 'errors' => array( 'Both current and new passwords are required.' ) );
        }
        
        //Hash the user pass
        $user_password_hash = $this->user_system_password_hash( $_POST['new_password'] );
        
        //If we've come from a reset, check that, otherwise run by user ID
        if( isset( $_SESSION['reset_key'] ) && !empty( $_SESSION['reset_key'] ) && !$this->logged_in() )
        {            
            $new = array(
                'user_password' => $user_password_hash, 
                'user_password_reset_hash' => 'NULL', 
                'user_password_reset_timestamp' => 'NULL'
            );
            $where = array(
                'user_password_reset_hash' => $_SESSION['reset_key']
            );
            $update = $this->database->update( $this->options['users_table'], $new, $where, 1 );
            
            //Kill the key
            unset( $_SESSION['reset_key'] );
        }
        elseif( $this->logged_in() )
        {
            $new = array(
                'user_password' => $user_password_hash, 
                'user_password_reset_hash' => 'NULL', 
                'user_password_reset_timestamp' => 'NULL'
            );
            $where = array(
                'user_id' => $_SESSION['user_id']
            );
            $update = $this->database->update( $this->options['users_table'], $new, $where, 1 );
        }
        
        if( isset( $update ) && $update > 0 )
        {
            return true;
        }
        
        return false;
        
    }
    //end update_password()


	/**
	 * @return array|bool
	 */
    public function password_reset()
    {
        if( !isset( $_POST['user_email'] ) || empty( $_POST['user_email'] ) )
        {
            return false;
        }
                
        //Find out if this user exists
        $user = $this->database->get_row( "SELECT user_id, user_name FROM ".$this->options['users_table']." WHERE LOWER(user_email) = ? LIMIT 1", array( strtolower( trim( $_POST['user_email'] ) ) ) );
        
        //No user, stop.
        if( empty( $user->user_id ) || empty( $user->user_name ) )
        {
            return array( 'success' => false, 'errors' => array( 'Invalid user.' ) );
        }
        
        $reset_hash = sha1( uniqid( mt_rand(), true ) );
        
        //Set the pass reset token in the db
        $update = array(
            'user_password_reset_hash' => $reset_hash, 
            'user_password_reset_timestamp' => time()
        );
        $where = array(
            'user_id' => $user->user_id, 
            'user_email' => $_POST['user_email']
        );
        if( $this->database->update( $this->options['users_table'], $update, $where, 1 ) )
        {
            //Send a message to the user
            if( $this->send_reset_email( $_POST['user_email'], $reset_hash ) )
            {
                return true;
            } 
        }
        
        return false;
    }
    //end password_reset()


	/**
	 * @return array|bool
	 */
    public function verify_pass_reset()
    {
        if( !isset( $_GET['code'] ) || empty( $_GET['code'] ) )
        {
            return array( 'success' => false, 'errors' => array( 'No verification code provided for reset.' ) );
        }
        
        
        $user = $this->database->get_row( "SELECT user_id, user_password_reset_timestamp FROM ".$this->options['users_table']." WHERE user_password_reset_hash = ? LIMIT 1", array( trim( $_GET['code'] ) ) );
        
        if( empty( $user->user_id ) || empty( $user->user_password_reset_timestamp ) )
        {
            return array( 'success' => false, 'errors' => array( 'invalid reset attempt' ) );
        }
        
        //Set the expired timestamp val
        $expiry_timestamp = time() - 3600;
        
        //We're within the hour, the reset is valid
        if( $user->user_password_reset_timestamp > $expiry_timestamp )
        {
            $_SESSION['reset_key'] = $_GET['code'];
            return true;
        }
        else
        {
            return array( 'success' => false, 'errors' => array( 'Reset duration has expired' ) );
        }
        return false;
    }
    //end verify_pass_reset()


	/**
	 * @return bool
	 */
	public function activate_account()
	{
		//Nothing to use, no reason to continue
		if( !isset( $_GET['code'] ) || empty( $_GET['code'] ) )
		{
			return false;
		}

		//Get the user ID
		$user = $this->database->get_row( "SELECT user_id FROM ".$this->options['users_table']." WHERE user_activation_hash = ? LIMIT 1", array( $_GET['code'] ) );
		if( empty( $user ) || empty( $user->user_id ) )
		{
			return false;
		}

		//Activate the user
		$update = array(
			'approved' => 1,
			'user_activation_hash' => 'NULL'
		);
		$where = array(
			'user_activation_hash' => $_GET['code']
		);
		$affected = $this->database->update( $this->options['users_table'], $update, $where, 1 );
		if( $affected > 0 )
		{
			return $user->user_id;
		}
		return false;
	}
	//end activate_account()


	/**
	 * @param string $uri
	 * @param string $message
	 * @return bool
	 */
	public function logout( $uri = '', $message = '' )
	{
		session_destroy();
		$_SESSION = array();
		$this->delete_cookie();

		if( empty( $uri ) || $uri == $this->options['url'] )
		{
			$uri = str_replace( '?logout=true', '', $this->options['url'] );
		}
		else
		{
			$append = '';
			if( urldecode( $uri ) != $this->options['url'] )
			{
				$append .= '?redirect='. urlencode( $uri );
			}
			$uri = $this->options['url'] . $this->options['secure_redirect'] . $append;
		}

		if( !empty( $message ) )
		{
			$this->flash( 'user_logout', $message );
		}
		else
		{
			$this->flash( 'user_logout', 'Logged out successfully!' );
		}

		if( !empty( $uri ) )
		{
			header( 'Location: '. $uri );
			exit;
		}

		//Default to true
		return true;
	}
	//end logout()


	/**
	 * @return bool
	 */
    public function logged_in()
    {
        if( isset( $_SESSION['user_logged_in'] ) && isset( $_SESSION['user_id'] ) && isset( $_SESSION['user_level'] ) )
        {
            return true;
        }
        elseif( $this->cookie_login() === true )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //end logged_in()


	/**
	 * @param string $uri
	 */
    public function page_protect( $uri = '' )
    {
        $uri = !empty( $uri ) ? $uri : $_SERVER['REQUEST_URI'];
        if( !$this->logged_in() )
        {
            //Cut any possible params that this user 'may not' be capable of having
            $this->logout( $uri );
        }
    }
    //end page_protect()


	/**
	 * Check for admin
	 * @access public
	 * @param int $id
	 * @return bool
	 */
	public function check_admin( $id = null )
	{
		if( !is_null( $id ) )
		{
			$user = $this->database->get_row( "SELECT user_level FROM ".$this->options['users_table']." WHERE user_id = ? LIMIT 1", array( $id ) );
			if( !empty( $user ) && isset( $user->user_level ) && $user->user_level >= $this->options['user_levels']['admin'] )
			{
				return true;
			}
			return false;
		}

		if( $this->logged_in() && isset( $_SESSION['user_level'] ) && $_SESSION['user_level'] >= $this->options['user_levels']['admin'] )
		{
			return true;
		}
		return false;
	}
	//end check_admin()


	/**
	 * Function to redirect non admin users back to normal user only areas
	 * @access public
	 * @param none
	 * @return mixed
	 */
	public function force_admin()
	{
		$this->page_protect();

		if( !$this->check_admin() )
		{
			$uri = !empty( $this->options['secure_redirect'] ) ? $this->options['url'] . $this->options['secure_redirect'] : $this->options['url'];
			redirect( $uri );
			exit;
		}
	}
	//end force_admin()


	/**
	 * @return bool
	 */
	public function get_user()
	{
		if( !$this->logged_in() )
		{
			return false;
		}

		$rs_settings = "SELECT * FROM ".$this->options['users_table']." WHERE user_id = ? LIMIT 1";
		return $this->database->get_row( $rs_settings, array( $_SESSION['user_id'] ) );
	}


    /**
     * @param $user_email
     * @param string $user_password
     * @param string $user_message
     * @return bool
     */
	private function send_reg_notice( $user_email, $user_password = '', $user_message = '' )
	{
		if( empty( $user_message ) )
		{
            $copy = $this->options['message_copy']['registration'];

			$message = $this->options['message_copy']['registration'];
		}
		else
		{
			$message = $user_message;
		}

        $args = [];
        //If we have message replacements defined, use them to replace values in the defined messages
        if( isset( $this->options['message_replacements'] ) && !empty( $this->options['message_replacements'] ) )
        {
            foreach( get_defined_vars() as $k => $v )
            {
                if( in_array( $k, array_values( $this->options['message_replacements'] ) ) )
                {
                    $replacement = array_search( $k, $this->options['message_replacements'] );
                    $args[$replacement] = $v;
                }
            }
        }
        $message = $this->parse_contents( $args, $message );

		if( $this->system_message( $user_email, $this->options['reg_success_subject'], $message ) )
		{
			return true;
		}
		return false;
	}
	//end send_reg_notice()


	/**
	 * @param int $user_id
	 * @param string $user_email
	 * @param string $user_activation_hash
	 * @return bool
	 */
	private function send_confirmation( $user_id, $user_email, $user_activation_hash )
    {
        $activation_link = '?user=' . urlencode( $user_id ) . '&code=' . urlencode( $user_activation_hash );
        $message = 'Please click on this link to activate your account: ';
        $message .= $this->options['url'] . $this->options['email_verification_url'] . $activation_link;

        if( $this->system_message( $user_email, $this->options['email_verification_subject'], $message ) )
        {
            return true;
        }
        return false;
    }
    //end send_confirmation()


	/**
	 * @param string $user_email
	 * @param string $reset_hash
	 * @return bool
	 */
    private function send_reset_email( $user_email, $reset_hash )
    {
        $link = $this->options['url'] . $this->options['email_password_reset_url'] . '?code=' . urlencode( $reset_hash );
        
        $message = 'Please click on this link to reset your password: ';
        $message .= ' '. $link;
        if( $this->system_message( $user_email, $this->options['email_password_reset_subject'], $message ) )
        {
            return true;
        }
        return false;
    }
    //end send_reset_email()


	/**
	 * @param string $email
	 * @param string $subject
	 * @param string $message
	 * @return bool
	 * @throws phpmailerException
	 */
    private function system_message( $email, $subject, $message )
    {
		if( function_exists( 'send_message' ) )
		{
			$from = array( 'from' => $this->options['from_email'], 'name' => $this->options['from_name'] );
			return send_message( $from, $email, $subject, $message, $reply_to = '', $extra = array() );
		}

        $mail = new PHPMailer;

        //Check to see if SMTP creds have been defined
        if( defined( 'SMTP_USER' ) && defined( 'SMTP_PASS' ) && defined( 'SMTP_LOCATION' ) && defined( 'SMTP_PORT' ) )
        {
            $mail->IsSMTP();
            $mail->Host = SMTP_LOCATION;
            $mail->SMTPAuth = true;
            $mail->Port = SMTP_PORT;
            $mail->Username = SMTP_USER;
            $mail->Password = SMTP_PASS;
        }

        $mail->From = $this->options['from_email'];
        $mail->FromName = $this->options['from_name'];
        $mail->addAddress( $email );

        $mail->WordWrap = 80;
        $mail->isHTML( true );

        $mail->Subject = $subject;
        $mail->Body = $message;

        if( !$mail->send() )
        {
            error_log( 'Mailer Error: ' . $mail->ErrorInfo );
            return false;
        }
        else
        {
            return true;
        }
    }
    //end system_message()



	/**
	 * Function to make sure usernames are unique
	 * Mainly a possible issue prevention for FB login/auth
	 * @access private
	 * @param string $username
	 * @return string $username
	 */
	public function generate_uniquename( $username )
	{
		$username = strtolower( preg_replace( '/[^A-Za-z0-9]+/', '', $username ) );
		$basecheck = $this->database->num_rows( "SELECT COUNT(user_id) FROM ".$this->options['users_table']." WHERE LOWER(user_name) = ?", array( strtolower( $username ) ) );
		if( $basecheck < 1 )
		{
			return $username;
		}

		$new_username = $username;
		$n = 0;
		do {
			$n = $n + 1;
			$new_username = strtolower( preg_replace( '/[^A-Za-z0-9]+/', '', $username . $n ) );
			$query = "SELECT COUNT(user_id) FROM ".$this->options['users_table']." WHERE LOWER(user_name) = ?";

			$post_name_check = $this->database->num_rows( $query, array( strtolower( $new_username ) ) );

		} while ( $post_name_check > 0 );

		return $new_username;
	}
	//end generate_uniquename()


	/**
	 * @param int $user_id
	 */
    private function set_remember_cookie( $user_id )
    {
        //Generate 64 char random string
        $random_token_string = hash( 'sha256', mt_rand() );
        
        //Update the token in the database
        $this->database->update( $this->options['users_table'], array( 'user_remember_token' => $random_token_string ), array( 'user_id' => $user_id ), 1 );

        //Generate cookie string that consists of user id, random string and combined hash of both
        $cookie_string_first_part = $user_id . ':' . $random_token_string;
        $cookie_string_hash = hash( 'sha256', $cookie_string_first_part );
        $cookie_string = $cookie_string_first_part . ':' . $cookie_string_hash;

        //Set the cookie
        setcookie( $this->options['cookie_name'], $cookie_string, time() + $this->options['cookie_runtime'], "/", $this->options['cookie_domain'] );
    }
    //end set_remember_cookie()


	/**
	 * @return bool
	 */
    private function delete_cookie()
    {
        setcookie( $this->options['cookie_name'], false, time() - (3600 * 3650), '/', $this->options['cookie_domain'] );
        return true;
    }
    //end delete_cookie()
    
    
    /**
     * Validate email addresses
     * @access public
     * @param string email
     * @return bool
     */
    public function user_valid_email( $address )
    {
        if( filter_var( $address, FILTER_VALIDATE_EMAIL ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //end user_valid_email()


	/**
	 * @param string $username
	 * @return bool
	 */
    public function user_valid_username( $username )
    {
        if( preg_match( '/^[a-z\d_]{5,20}$/i', $username ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //end user_valid_username()


	/**
	 * Get accurate IP Address of users
	 *
	 * @access public
	 * @param none
	 * @return string
	 *
	 */
	public function user_system_get_ip()
	{
		if( isset( $_SERVER ) )
		{
			//Handle cloudflare referral IP business
			if( isset( $_SERVER['HTTP_CF_CONNECTING_IP'] ) )
			{
				$realip = $_SERVER['HTTP_CF_CONNECTING_IP'];
			}
			elseif( isset( $_SERVER["HTTP_X_FORWARDED_FOR"] ) )
			{
				$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
			}
			elseif( isset( $_SERVER["HTTP_CLIENT_IP"] ) )
			{
				$realip = $_SERVER["HTTP_CLIENT_IP"];
			}
			else
			{
				$realip = $_SERVER["REMOTE_ADDR"];
			}
		}
		else
		{
			if( getenv( 'HTTP_X_FORWARDED_FOR' ) )
			{
				$realip = getenv( 'HTTP_X_FORWARDED_FOR' );
			}
			elseif( getenv( 'HTTP_CLIENT_IP' ) )
			{
				$realip = getenv( 'HTTP_CLIENT_IP' );
			}
			else
			{
				$realip = getenv( 'REMOTE_ADDR' );
			}
		}
		return $realip;
	}
	//end user_system_get_ip()


	/**
	 * @param string $name
	 * @return array|bool
	 */
    public function get_option( $name = '' )
    {
        if( empty( $name ) )
        {
            return $this->options;
        }
        
        if( isset( $this->options[$name] ) )
        {
            return $this->options[$name];
        }
        else
        {
            return false;
        }
    }
    //end get_option()
    
    
    /**
     * Hash Passwords for storage
     *
     * @access private
     * @param string password
     * @return string
     */
    public function user_system_password_hash( $password )
    {
        if( isset( $this->options['use_pwd_hash'] ) && $this->options['use_pwd_hash'] === true )
        {
            return password_hash( $password, PASSWORD_DEFAULT, array( 'cost' => $this->options['hash_cost_factor'] ) );
        }

        $unique_salt = substr( sha1( mt_rand() ), 0, 22 );
        return crypt( $password, '$2a$10$'. $unique_salt );
    }


    /**
     * Function to evaluate password match between hashed and nonhashed values
     * Example:
     * if( $this->user_check_password( $pass_hash, $_POST['login-password'] ) )
     * {
     *    echo 'Matched!';
     * }
     *
     * @access private
     * @param $hash db stored hashed password
     * @param $password usually $_POST value from login form
     * @return bool
     */
    public function user_check_password( $hash, $password )
    {
        if( isset( $this->options['use_pwd_hash'] ) && $this->options['use_pwd_hash'] === true )
        {
            return password_verify( $password, $hash );
        }

        /* alternate method*/
         
         $full_salt = substr( $hash, 0, 29 );
         $new_hash = crypt( $password, $full_salt );
         if( $hash == $new_hash )
         {
             return true;
         }
        return false;
    }
    //end user_check_password()


	/**
	 * Function to create and display error and success messages
	 * @access public
	 * @param string session name
	 * @param string message
	 * @param string display class
	 * @return string message
	 */
	public function flash( $name = '', $message = '', $class = 'success' )
	{
		//If the function is available from core, use it instead
		if( function_exists( 'flash' ) )
		{
			flash( $name, $message, $class );
			return;
		}

		if( !empty( $name ) )
		{
			//No message, create it
			if( !empty( $message ) && empty( $_SESSION[$name] ) )
			{
				if( !empty( $_SESSION[$name] ) )
				{
					unset( $_SESSION[$name] );
				}
				if( !empty( $_SESSION[$name.'_class'] ) )
				{
					unset( $_SESSION[$name.'_class'] );
				}

				$_SESSION[$name] = $message;
				$_SESSION[$name.'_class'] = $class;
			}
			//Message exists, display it
			elseif( !empty( $_SESSION[$name] ) && empty( $message ) )
			{
				$class = !empty( $_SESSION[$name.'_class'] ) ? $_SESSION[$name.'_class'] : 'success';
				echo '<div class="alert-box radius '.$class.'" id="msg-flash">'.$_SESSION[$name].'</div>';

				unset( $_SESSION[$name] );
				unset( $_SESSION[$name.'_class'] );
			}
		}
	}


    /**
     * @param $val
     * @return string
     */
    private function map_replacement( $val )
    {
        return '({'.$val.'})';
    }


    /**
     * @param array $data
     * @param string $contents
     * @return mixed
     */
    private function parse_contents( $data, $contents )
    {
        $replace = array_keys( $data );
        $replace = array_map( array( $this, 'map_replacement' ), $replace );
        $replacements = array_values( $data );
        //Replace the codetags with the message contents
        return preg_replace( $replace, $replacements, $contents );
    }
    
    
    public function __destruct()
    {
        //
    } //end __destruct()
    
} //end userClass