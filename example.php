<?php

require_once( 'classes/SimplePDO.php' );
require_once( 'classes/userClass.php' );
$params = array(
    'host' => 'localhost', 
    'user' => 'root', 
    'password' => 'root', 
    'database' => 'rapidphp'
);
//Set the options
SimplePDO::set_options( $params );

//Set the class options
$options = array(
    //require email conf. for normal reg?
    'confirm_users' => false,

    //require email conf. for social media auth?
    'confirm_social_users' => false, 
    
    //send users a reg success email?
    'send_reg_success' => true, 

    //database table name to use for users
    'users_table' => 'users',  

    //Force header() location redirect?           
    'header_redirect' => false,

    //Reg with email OR username/email
    'email_only' => true,
    
    //If Crypt functions are availble, use them?
    'use_crypt' => true,

    'use_pwd_hash' => true,

    //Trailing slash IS important!
    'url' => 'http://yoursite.com/',

    //Multiplier for password hashing
    'hash_cost_factor' => 10,

    //2 weeks for secure cookie
    'cookie_runtime' => 1209600, 

    // important: always put a dot in front of the domain, like ".mydomain.com" see http://stackoverflow.com/q/9618217/1114320
    'cookie_domain' => '.' . $_SERVER['HTTP_HOST'], 

    //Name of cookie to set
    'cookie_name' => 'remember_me', 

    //Outbound email "from" email
    'from_email' => 'no-reply@'. $_SERVER['HTTP_HOST'] .'.com', 

    //Outbound email "from" name
    'from_name' => 'Your Site', 

    //Full URI to where you plan on having the activate_account function
    'email_verification_url' => '', 

    //Subject line for account activation emails
    'email_verification_subject' => 'Account activation', 
    
    'reg_success_subject' => 'Account created successfully', 

    //Password reset URI where verify_pass_reset() will be
    'email_password_reset_url' => '', 

    //Password reset email subject
    'email_password_reset_subject' => 'Password reset', 

    //Logout redirect for page_protect() (aka, login page)
    'secure_redirect' => 'users/login', 

    //Oauth credentials
    'oauth' => array(
        'fb' => array(
            'client_id' => '', 
            'client_secret' => '', 
            'client_redirect' => 'users/authorize/facebook/'
        ), 
        'google' => array(
            'client_id' => '', 
            'client_secret' => '', 
            'client_redirect' => 'users/authorize/google/'
        )
    ), 
    
    //User levels
    'user_levels' => array(
        'user' => 1, 
        'admin' => 5
    ),

    //Email messaging copy
    'message_copy' => array(

        //Registration without confirmation
        'registration' => '',

        //Registration WITH confirmation
        'registration_confirm' => '',

        //Password reset messages
        'password_reset' => '',

    ),


    'message_replacements' => array(

        'email' => 'user_email',

        'password' => 'user_password',
    ),
);

$users = new userClass( SimplePDO::getInstance(), $options );

//$users->page_protect();

//Get all the options set
/*
$options = $users->get_option();
echo '<pre>';
print_r( $options );
echo '</pre>';
exit;
*/

/*******************************
 Register a new user
********************************/
/*
$_POST = array(
    'user_name' => 'booooyahhhh',
    'user_email' => 'bennettstone22@gmail.com',  
    'user_password' => '12345678', 
    'user_remember' => true
);

$reg = $users->register();
echo '<pre>';
print_r( $reg );
echo '</pre>';
exit;
*/

/*******************************
 Register/login a user with facebook
 For this, you'll want to do some extra checks due to the redirect path
 required to authorize a user with social media
 
 For example, have a page that has nothing BUT the below code on it,
 or similar
********************************/
if( isset( $_GET['facebook_login'] ) || isset( $_GET['code'] ) )
{
    $users->facebook_authenticate();
}


/*******************************
 Register/login a user with google
 For this, you'll want to do some extra checks due to the redirect path
 required to authorize a user with social media
 
 For example, have a page that has nothing BUT the below code on it,
 or similar
********************************/

if( isset( $_GET['google_login'] ) || isset( $_GET['state'] ) )
{
    $data = $users->google_authenticate();
    echo '<pre>';
    print_r( $data );
    echo '</pre>';
}

/*******************************
 Activate a user
********************************/
/*
$_GET = array(
    'user' => 83, 
    'code' => 'cc56f51c21193b456ffa3690fd939a9c6a88dc50'
);
$activated = $users->activate_account();

echo '<pre>';
print_r( $activated );
echo '</pre>';
*/

/*******************************
 Login a user
********************************/
/*
$_POST = array(
    'user_email' => 'bennettstone22@gmail.com', 
    'user_password' => '12345678', 
    'user_remember' => true, 
    'redirect' => 'http://google.com'
);

OR...

$_POST = array(
    'user_email' => 'bennettstone22@gmail.com', 
    'user_name' => 'bennettman', 
    'user_password' => '12345678', 
    'user_remember' => true, 
    'redirect' => 'http://google.com'
);

$login = $users->login();

echo '<pre>';
print_r( $login );
echo '</pre>';
exit;
*/

/*******************************
 Logout a user
 Usually helpful to make sure a user is actually logged in first ;)
********************************/
if( isset( $_GET['logout'] ) && $users->logged_in() && $users->logout() )
{
    header( 'Location: '. $users->get_option( 'url' ) );
    exit;
}

/*******************************
 Request password reset
********************************/
/*
$_POST = array(
    'user_name' => 'someotherguy'
);

$reset = $users->password_reset();
echo '<pre>';
print_r( $reset );
echo '</pre>';
exit;
*/
/*******************************
 Verify a password reset attempt
 Must be followed by the setting of a new password
********************************/
/*
$_GET = array(
    'code' => '9285a4da26333e58de8847eb8283523c737e029c'
);
$reset = $users->verify_pass_reset();
echo '<pre>';
print_r( $reset );
echo '</pre>';
exit;
*/


echo '<pre>';
print_r( $_SESSION );
echo '</pre>';
echo '<pre>';
print_r( $_COOKIE );
echo '</pre>';